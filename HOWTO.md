
# Adding tests to the Upgrade2StackTester through the new Config System
This repository simulataneously provides a service to build and run custom stack builds from [lb-stack-setup](https://gitlab.cern.ch/rmatev/lb-stack-setup) on the grid as well as simply running released/nightly LHCbPR jobs in grid mode. If you want to run released/nightly software only, skip to section [How to add a Job to the Schedule].

The major advantage of this tool is that it will allow longer and larger LHCbPR jobs to be run by splitting tests over many grid nodes. This also applies to the stack tests, where a built stack is uploaded to the grid and can be run in a split fashion to produce large samples quickly.

It also allows the chaining of jobs using LHCbTasks, allowing different applications to be run sequentially.

For now, we ask testers to branch the repo and put in a merge request to master.

## Building a custom stack
The system takes a list of commit hashes, a platform and a list of projects to build and will run the setup in [lb-stack-setup](https://gitlab.cern.ch/rmatev/lb-stack-setup). If successful after some initial checksd the stack is cleaned and zipped to retain only the neccesary files to run jobs, reducing the size of a normal stack from ~10GB to ~250MB. This is converted into an exectuable and uploaded to the grid ready for use in ganga.

It is intended for testing new simulation and reconstruction methods that are yet to be released, perfect for prototype detector layouts or reconstruction methods for upgrade 2 studies.

To add a new custom stack, a new entry in [StackCreator.json](StackCreator.json) should be added, and the following specified:
- name : The unique name of the stack to be specified in jobs
- binaryTag : platform for stack to be built on
- lcgVersion : LCG version to build the stack in
- checkouts : A dictionary of commit hashes for the different repositories built in the stack. Commits are advised instead of branches for stable builds. **Advice is also to "over-specify" with changing versions of master potentially causing issues in the stack builds.** From the root of your test stack, running `make for-each CMD="git rev-parse --short HEAD"` will list all of the (short) commit hashes for the stack in it's current state (make sure they're published!). Please check your mixture builds beforehand!
- projectsToBuild : Which jobs may be run with this stack, usually Gauss and/or Moore
More options to come... feel free to suggest!

with an example below.
```json
{
    "name":"U2_TV",
    "binaryTag":"x86_64_v2-centos7-gcc12-opt",
	"lcgVersion" : "103",
    "checkouts": {
    "Detector":"72929eb",
    "Gaudi":"1bf778e",
    "Gauss":"14d3673",
    "Gaussino":"488dbbf",
    "LHCb":"1f9072b",
    "Rec":"ffae5bc",
    "Allen":"6d2fbcd",
    "Moore":"19296bf"
    },
    "projectsToBuild":["Moore", "Gauss"]
}
```
To update a job with new commits, please edit your stack in place. This will prompt the launching of the jobs that use this stack.

## Adding a new job
The jobs run is controlled by [TestJobSchedule.json](TestJobSchedule.json) . This is where details and settings about the job are specified. This works in a similar style to LHCbPR where a schedule of when to run the job can be specified as well as which handler should be run upon job completion.

The job options specified must either be paths to be accessed from within the application (e.g \\$DECFILESROOT/options/NNNNNNNN.py) or **paths to within a new sub-folder(s) added by the user relative to the dir [/TestJobScripts](/TestJobScripts)** (see the example below).

To add a new job, add a new entry to [TestJobSchedule.json](TestJobSchedule.json) with the following specified (example below):

- jobTitle : The unique title for these jobs (for the name should be suffixed with "-queue")
- days : List of days per week this job should be run. For stack jobs, the job will automatically be run upon a change to the stack as mentioned, so leaving this as "once" is desirable (stackTimeStamp). (Note: this may not work right now, please add "Once" for now)
- time : When to run this in the days specified
- platform : The platform to run the job in, for stack jobs please ensure this is the same as the stack's binary tag
- stackName : Having this set to anything but "" will launch the job in "Stack" mode using the stack named here. (If running released or nightly software, set this to "")
- stackTimestamp : (IN TESTING) If the job should run on an older version of the stack specified, if this is "", the job will use the latest version of the stack. (If running released or nightly software, set this to "")
- nightlyFlavour : If running "classic" LHCbPR tests, specify nightly flavour to run (If running fully released software or stack set this to "").
- nightlySlot : Specific nightly slot to run the tests, as with LHCbPR "latest" and "yesterday" also work. (If running fully released software or stack set this to "")
- PRJobOptions : The options files to run the job (`application/run gaudirun.py ...`). These either must be within the application, or placed in [/TestJobScripts](/TestJobScripts) within a folder and accessed relatively as seen in example below.
- PRExtractInputScript : (EXPERIMENTAL, for LHCbPR jobs) Provide the path to an extra script that prints out a list of the input files from within the application, to allow ganga to split jobs by grid file location. (reccommend using inputFiles instead!)
- PRRemoveInputOption : (EXPERIMENTAL, for LHCbPR jobs) If a job option file has a specified input file list, this should remove it.
- inputFiles : If the job is individual or the first in a chain, this is the explicit input files in the format of either a bkpath, a list of PFNs or a list of LFNs. If the job is the second in a chain, see below.
- outputFileTypes : List of output file extensions that should be retained by the jobs, be used by a handler or saved.
- splitJob : How many sub-jobs to split this job into
- handlerToRun : Specify the name of the handler to run upon this jobs completion. It is recommended to run an LHCbPR handler over your job output.
- stackOutGridLoc : (To be implemented...) **For custom stack jobs only**, a writeable eos/grid location for your specifed output files to be uploaded if a handler is not available/wanted
- application : The application to run the job on. For stack jobs this must match the built project name. 
- applicationVer : If using released software, this is a version number, if using nightly software this can point to a branch e.g. master, head etc.. For stack jobs leave as "".
- requiresJob : For chained jobs specify the "jobTitle" of the previous stage needed (see below)
- newDir : A directory name to run the jobs in on the system, generally this is the "jobTitle" without the queue, use the same for a series of chained jobs.
- newDevDir : For testing, leave as "DEVDIR" for now
```json
{
	"jobTitle":"U2_MinBias_nu60_10K_TVOnly_75um3p5mmCylindFoil_Gauss-queue",
	"days":[], //note this should be ["once"] to ensure the job is run
	"time":10,
	"platform":"x86_64_v2-centos7-gcc12-opt",
	"stackName":"U2_TV",
	"stackTimestamp":"310124",
	"nightlyFlavour":"",
	"nightlySlot":"",
	"PRJobOptions":"\\$DECFILESROOT/options/30000000.py U2TVStack_MinBias_nu60_FoilOptions/UseBeamSpot4D_sim11.py U2TVStack_MinBias_nu60_FoilOptions/Beam7000GeV-md100-nu60-VerExtAngle.py U2TVStack_MinBias_nu60_FoilOptions/Gauss-TVOnly-Sim11.py U2TVStack_MinBias_nu60_FoilOptions/Gauss-TV_Setup_CylindFoil75um.py U2TVStack_MinBias_nu60_FoilOptions/10KEvents.py",
	"PRExtractInputScript": "",
	"PRRemoveInputOption":"",
	"inputFiles":[],
	"outputFileTypes":[".root"],
	"splitJob":100,
	"handlerToRun":"",
    "stackOutGridLoc":"",
	"application":"Gauss",
	"applicationVer":"",
	"requiresJob":"",
	"newDir":"U2_MinBias_nu60_10K_TVOnly_75um3p5mmCylindFoil",
	"newDevDir":"DEVDIR"
},
```

### Chaining Jobs
The system uses LHCbTasks to allow chaining of jobs which enables simulation -> reconstruction chains and similar tests.  See an example below for a Moore job chaining from the above Gauss Job
This works in "reverse"; the top level job is launched and checks "requiresJob", launching this and any subsequent jobs until "requiresJob" = "". (the Moore job below triggers the launching of the Gauss job above, note how the Moore job has "days"=["Once"] and is the actual job the system launches)
To facilitate the passing of output and input files between transforms in LHCbTask, the inputFiles of one job must describe the file extension of the output file of the previous step that is needed. Ganga will then manage the transfer of this intermediate file (in this case the .SIM file) and delete it upon completion of the job (as .SIM is not specified in the outputFileTypes of the Gauss job above).

```json
{
	"jobTitle":"U2_MinBias_nu60_10K_TVOnly_75um3p5mmCylindFoil_10um50ps_Moore-queue",
	"days":["Once"],
	"time":15,
	"platform":"x86_64_v2-centos7-gcc12-opt",
	"stackName":"U2_TV",
	"stackTimestamp":"310124",
	"nightlyFlavour":"",
	"nightlySlot":"",
	"PRJobOptions":"U2TVStack_Moore_FoilOptions/hlt1_TV_CylindFoil75um.py U2TVStack_Moore_FoilOptions/hlt1_TV_allreco_PRReady.py",
	"PRExtractInputScript": "",
	"PRRemoveInputOption":"",
	"inputFiles":[".SIM"],
	"outputFileTypes":[".root"],
	"splitJob":100,
	"handlerToRun":"U2_Velo_MinBiasHandler",
	"application":"Moore",
	"applicationVer":"",
	"requiresJob":"U2_MinBias_nu60_10K_TVOnly_75um3p5mmCylindFoil_Gauss-queue",
	"newDir":"U2_MinBias_nu60_10K_TVOnly_75um3p5mmCylindFoil",
	"newDevDir":"DEVDIR"
},
```

### Handlers
It is recommended to run the output of these jobs on an LHCbPRHandler, pushed to [LHCbPR2HD](https://gitlab.cern.ch/lhcb-core/LHCbPR2HD/-/tree/LHCbPR_OTG_TestHandlers?ref_type=heads), to automatically analyse and present the results on the lhcbpr website: https://epgr03.ph.bham.ac.uk/ (NOTE: currently fixing a small issue where mixed-content must be enabled).

To add your own handler, put a merge request onto the LHCbPR_OTG_TestHandlers branch of LHCbPR2HD and specify the name of the created handler in "handlerToRun".

The handler will take a directory as it's input (see the [readme.md](https://gitlab.cern.ch/lhcb-core/LHCbPR2HD/-/blob/LHCbPR_OTG_TestHandlers/README.md?ref_type=heads)), within which will be all of the job's log files and outputs with the following naming convention:
- Log files are named "log_N.txt" where N is the sub-job number, and contains the full output from the job run
- Any other output files are their nominal names with "_N" appended before the file extension, and are listed at the bottom of the log files created below a line containing "\*\*Saved Output Files\*\*"
An example can be seen here: [U2_Velo_MinBiasHandler.py](https://gitlab.cern.ch/lhcb-core/LHCbPR2HD/-/blob/LHCbPR_OTG_TestHandlers/handlers/U2_Velo_MinBiasHandler.py?ref_type=heads). 

The handler will **currently** need to combine all of the output files together during processing, by opening the in a TChain or similar.

[WIP:] An extra option "haddRoot" is being considered that will automatically combine any root files specified, this will allow old handlers to be reused for the split jobs.

### Saving the output (Stack jobs only): TO BE IMPLEMENTED
Alternatively, if analysis and presentation of results is not the desired use of your stack jobs, a writeable grid/eos location can be provided (soon). The files detected and picked up by outputFileTypes will then be moved to the location specified.
This is only available for Stack jobs, any LHCbPR jobs will ignore this entry (as they require a handler).

### Note on python / executable applications : TO BE WRITTEN
See "U2_Bs2DsPi_Selections-queue" and the linked jobs for guide of how to run python/excutable jobs.

### Notes on "Classic" LHCbPR jobs on the grid : TO BE WRITTEN
